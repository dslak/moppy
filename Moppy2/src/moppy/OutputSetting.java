
package moppy;

import java.io.Serializable;

public class OutputSetting implements Serializable{
    public enum OutputType {MOPPY, MIDI};

    public final int MIDIChannel;
    public boolean enabled = false;
    public OutputType type = OutputType.MOPPY;
    public String comPort;
    public String midiDeviceName;

    public OutputSetting(int MIDIChannel){
        this.MIDIChannel = MIDIChannel;
    }
}
